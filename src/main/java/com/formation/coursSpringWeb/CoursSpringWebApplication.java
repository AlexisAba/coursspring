package com.formation.coursSpringWeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class CoursSpringWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoursSpringWebApplication.class, args);
	}

}
