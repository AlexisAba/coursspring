package repository;

import model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Produit, Integer> {
	Produit findByNameAndPrice (
			@Param("name") String nom,
			@Param("price") int prix
			);

}
