package configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.formation.coursSpringWeb.CoursSpringWebApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = CoursSpringWebApplication.class)
public class CoursSpringWebApplicationConfigurationTest {
	
	@Test
	public void bootstrapsApplication() {
		new AnnotationConfigApplicationContext(CoursSpringWebApplication.class);
	}

}
