package repository;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.formation.coursSpringWeb.CoursSpringWebApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProductRepositoryTest {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Test
	public void testFinAll_andProducerWorked() {
		assertThat(productRepository.findAll()).hasSize(1);
	}
	
	@Test
	public void findByNameAndPrice_andNoneFound() {
		assertThat(productRepository.findByNameAndPrice("Android", 10)).isNull();
	}
	
	@Test
	public void findByNameAndPrice_andFoundOne() {
		assertThat(productRepository.findByNameAndPrice("IPhone", 10)).isNotNull();
	}
}
